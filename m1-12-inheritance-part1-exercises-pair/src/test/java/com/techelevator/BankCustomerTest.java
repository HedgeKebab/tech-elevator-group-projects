package com.techelevator;

import org.junit.Assert;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BankCustomerTest {
	
	BankCustomer bankcustomer;
	
@Before
public void setup() {
	bankcustomer = new BankCustomer();
}

@Test
public void verify_name() {
	bankcustomer.setName("Don");
	Assert.assertEquals("name is incorrect: " + bankcustomer.getName(), "Don", bankcustomer.getName());
}

@Test
public void verify_address() {
	bankcustomer.setAddress("12359");
	Assert.assertEquals("address doesn't match: " + bankcustomer.getAddress(), "12359", bankcustomer.getAddress());
}

@Test
public void verify_phone_number() {
	bankcustomer.setPhoneNumber("6145551234");
	Assert.assertEquals("phone number doesn't match: " + bankcustomer.getPhoneNumber(), "6145551234", bankcustomer.getPhoneNumber());
}

@Test
public void verify_get_accounts() {
	bankcustomer.getAccounts();
	
	Assert.assertEquals("no bank accounts: " + bankcustomer.getAccounts().size(), 0, bankcustomer.getAccounts().size());
}

@Test
public void verify_account_added() {
	BankAccount slush = new BankAccount();
	bankcustomer.addAccount(slush);

	Assert.assertEquals("account failed to add: " + bankcustomer.getAccounts().size(), 1, bankcustomer.getAccounts().size());
}
@Test
public void verify_multiple_accounts_added() {
	BankAccount slush = new BankAccount();
	BankAccount Eddie = new BankAccount();
	BankAccount George = new BankAccount();
	
	bankcustomer.addAccount(slush);
	bankcustomer.addAccount(Eddie);
	bankcustomer.addAccount(George);
	
	Assert.assertEquals("account failed to add: " + bankcustomer.getAccounts().size(), 3, bankcustomer.getAccounts().size());
}

@Test
public void verify_no_vip() {
	BankAccount slush = new BankAccount();
	bankcustomer.addAccount(slush);

	boolean success = bankcustomer.isVIP();
	Assert.assertFalse("is a VIP", success);
}

@Test
public void verify_vip_status() {
	BankAccount slush = new BankAccount();
	BankAccount Eddie = new BankAccount();
	
	bankcustomer.addAccount(slush);
	bankcustomer.addAccount(Eddie);
	
	slush.deposit(new BigDecimal(10000));
	Eddie.deposit(new BigDecimal(20000));
	
	boolean success = bankcustomer.isVIP();
	
	Assert.assertTrue("is not VIP", success);
}

@Test
public void verify_account_balance() {
	BankAccount slush = new BankAccount();
	
	bankcustomer.addAccount(slush);
	
	slush.deposit(new BigDecimal(10000));
	Assert.assertEquals("account failed to add: " + bankcustomer.confirmAccountsBalance(), 10000, bankcustomer.confirmAccountsBalance(), 0.0);
}

}
