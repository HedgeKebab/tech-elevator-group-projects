package com.techelevator;

import org.junit.Assert;
import java.math.BigDecimal;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CheckingAccountTest {
	
	CheckingAccount bankaccount;
	
	@Before
	public void setup() {
		bankaccount = new CheckingAccount();
	}
	
	@Test
	public void verify_withdraw_succeeds() {
		bankaccount.deposit(new BigDecimal(1000));
		bankaccount.withdraw(new BigDecimal(100));
		Assert.assertEquals("withdraw failed: " + bankaccount.getBalance(), new BigDecimal(900), bankaccount.getBalance());
	}

	@Test
	public void verify_withdraw_penalty() {
		bankaccount.deposit(new BigDecimal(100));
		bankaccount.withdraw(new BigDecimal(110));
		Assert.assertEquals("withdraw failed: " + bankaccount.getBalance(), new BigDecimal(-20), bankaccount.getBalance());
	}
	
	@Test
	public void verify_cannot_overdraw() {
		bankaccount.deposit(new BigDecimal(10));
		bankaccount.withdraw(new BigDecimal(130));
		Assert.assertEquals("withdraw failed: " + bankaccount.getBalance(), new BigDecimal(10), bankaccount.getBalance());
	}
}
