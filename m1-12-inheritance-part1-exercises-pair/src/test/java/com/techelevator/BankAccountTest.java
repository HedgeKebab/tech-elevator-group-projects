package com.techelevator;

import org.junit.Assert;
import java.math.BigDecimal;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class BankAccountTest {

	BankAccount bankaccount;
	
	@Before
	public void setup() {
		bankaccount = new BankAccount();
	}
	
	@Test
	public void verify_initial_account_balance() {
		Assert.assertEquals("balance does not begin at zero: " + bankaccount.getBalance(), new BigDecimal(0), bankaccount.getBalance());
	}
	
	@Test
	public void verify_amount_deposits() {
		bankaccount.deposit(new BigDecimal(50));
		Assert.assertEquals("amount deposited failed to go through: " + bankaccount.getBalance(), new BigDecimal(50),bankaccount.getBalance());
		
	}
	
	@Test
	public void verify_amount_withdraws() {
		bankaccount.deposit(new BigDecimal(100));
		bankaccount.withdraw(new BigDecimal(50));
		Assert.assertEquals("amount withdrawn unexpected: " + bankaccount.getBalance(), new BigDecimal(50), bankaccount.getBalance());
	}
	
	@Test
	public void verify_transfer_succeeds() {
		bankaccount.deposit(new BigDecimal(100));
		BankAccount destinationAccount = new BankAccount();
		bankaccount.transfer(destinationAccount, new BigDecimal(50));
		Assert.assertEquals("amount failed to transer: " + destinationAccount.getBalance(), new BigDecimal(50), destinationAccount.getBalance());
	}
	
	@Test
	public void verify_get_account_number() {
		bankaccount.setAccountNumber("441409");
		Assert.assertEquals("invalid account number: " + bankaccount.getAccountNumber(), "441409", bankaccount.getAccountNumber());
	}
}
