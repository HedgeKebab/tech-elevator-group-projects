package com.techelevator;

import org.junit.Assert;
import java.math.BigDecimal;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SavingsAccountTest {
		
		SavingsAccount bankaccount;
		
	@Before
	public void setup() {
		bankaccount = new SavingsAccount();
	}
	
	@Test
	public void verify_withdraw_succeeds() {
		bankaccount.deposit(new BigDecimal(1000));
		bankaccount.withdraw(new BigDecimal(100));
		Assert.assertEquals("withdraw failed: " + bankaccount.getBalance(), new BigDecimal(900), bankaccount.getBalance());
	}
	
	@Test
	public void verify_penalty_amount() {
		bankaccount.deposit(new BigDecimal(150));
		bankaccount.withdraw(new BigDecimal(50));
		Assert.assertEquals("penalty amount error: " + bankaccount.getBalance(), new BigDecimal(98), bankaccount.getBalance());
	}
	
	@Test
	public void verify_cannot_overdraw() {
		bankaccount.deposit(new BigDecimal(1500));
		bankaccount.withdraw(new BigDecimal(2000));
		Assert.assertEquals("account overdrawn: " + bankaccount.getBalance(), new BigDecimal(1500),  bankaccount.getBalance());
		
	}
		
}
