package com.techelevator;

import java.math.BigDecimal;

public class SavingsAccount extends BankAccount{
	

	@Override
	public BigDecimal withdraw(BigDecimal amountToWithdraw) {
		BigDecimal pull = this.getBalance().subtract(amountToWithdraw);
		
		if (amountToWithdraw.intValue() > this.balance.intValue()) {
		
			return this.balance;
		}
			
		else if (balance.intValue() <= 150) {
			this.balance = pull.subtract(new BigDecimal(2));
			return this.balance;
		}
		
		else {
		this.balance = pull.subtract(new BigDecimal(0));
		return this.balance;
		}
	}
}
