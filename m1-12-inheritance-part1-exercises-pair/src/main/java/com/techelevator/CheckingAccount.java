package com.techelevator;

import java.math.BigDecimal;

public class CheckingAccount extends BankAccount {
	
	@Override
	public BigDecimal withdraw(BigDecimal amountToWithdraw) {
		BigDecimal pull = this.getBalance().subtract(amountToWithdraw);

		
		if (pull.intValue() <= -100) {
			return this.balance;
		}
		
		else if (pull.intValue() < 0) {
		this.balance = pull.subtract(new BigDecimal(10));
		return this.balance;
		}
		
		else {
			this.balance = pull.subtract(new BigDecimal(0));
			return this.balance;
		}
	}

}
