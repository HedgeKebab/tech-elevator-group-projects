package com.techelevator;

import java.util.List;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;

public class BankCustomer implements VIP{
	
	private String name;
	private String address;
	private String phoneNumber;
	private ArrayList<BankAccount> accounts = new ArrayList<BankAccount>();
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getAddress() {
		return this.address;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPhoneNumber() {
		return this.phoneNumber;
	}
	
	public ArrayList<BankAccount> getAccounts() {
		return this.accounts;
	}
	
	public ArrayList<BankAccount> addAccount(BankAccount newAccount) {
		this.accounts.add(newAccount);
		return this.accounts;
	}
	
	public double confirmAccountsBalance() {
		double sum = 0;
		for (BankAccount vault : accounts) {
			BigDecimal subTotal = vault.balance;
			sum += subTotal.doubleValue();
		}
		return sum;
	}

	@Override
	public boolean isVIP() {
		double sum = 0;
		for (BankAccount vault : accounts) {
			BigDecimal subTotal = vault.balance;
			sum += subTotal.doubleValue();
		}
		
		if (sum >= 25000.00) {
			return true;
		}
		else {
			return false;
		}
	}
}
