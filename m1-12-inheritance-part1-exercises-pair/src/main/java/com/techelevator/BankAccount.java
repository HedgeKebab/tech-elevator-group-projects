package com.techelevator;

import java.math.BigDecimal;
public class BankAccount {

	private String accountNumber;
	protected BigDecimal balance;
	
	public BankAccount(){
		//this.balance = balance;
		this.balance = new BigDecimal(0);
	}
	
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getAccountNumber() {
		return this.accountNumber;
	}

	public BigDecimal getBalance() {
		return this.balance;
	}
	
	public BigDecimal deposit(BigDecimal amountToDeposit) {
		this.balance = this.balance.add(amountToDeposit);
		return this.balance;
	}
	
	public BigDecimal withdraw(BigDecimal amountToWithdraw) {
		this.balance = this.balance.subtract(amountToWithdraw);
		return this.balance;
	}
	
	public void transfer(BankAccount destinationAccount, BigDecimal transferAmount) {
		this.balance = this.balance.subtract(transferAmount);
		destinationAccount.balance = destinationAccount.balance.add(transferAmount);
	}
}
