package com.techelevator;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;


public class VolunteerWorkerTest {
	
	VolunteerWorker worker;
	
	@Before
	public void setup() {
		worker = new VolunteerWorker("Bill", "Mahr");
	}
	
	@Test
	public void verify_first_name() {
		Assert.assertEquals("first name incorrect: " + worker.getFirstName(), "Bill", worker.getFirstName());
	}

	@Test
	public void verify_last_name() {
		Assert.assertEquals("last name incorrect: " + worker.getLastName(), "Mahr", worker.getLastName());
	}
	
	@Test
	public void verify_pay() {
		worker.calculateWeeklyPay(5);
		Assert.assertEquals("pay incorrect: " + worker.calculateWeeklyPay(5), 0, worker.calculateWeeklyPay(5), 0.0);
		
		
	}
}
