package com.techelevator;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;


public class HourlyWorkerTest {

	
	HourlyWorker worker;

	@Before
	public void setup() {
		worker = new HourlyWorker("Greg", "Mack", 1000);
	}
	
	@Test
	public void verify_first_name() {
		Assert.assertEquals("first name incorrect: " + worker.getFirstName(), "Greg", worker.getFirstName());
	}

	@Test
	public void verify_last_name() {
		Assert.assertEquals("last name incorrect: " + worker.getLastName(), "Mack", worker.getLastName());
	}
	
	@Test
	public void verify_hourly_rate() {
		worker.getHourlyRate();
		Assert.assertEquals("hourly rate incorrect: " + worker.getHourlyRate(), 1000, worker.getHourlyRate(), 0.0);
	}
	
	@Test
	public void verify_pay() {
		worker.calculateWeeklyPay(40);
		Assert.assertEquals("pay incorrect: " + worker.calculateWeeklyPay(40), 40000, worker.calculateWeeklyPay(40), 0.0);
	}
	
	@Test
	public void verify_overtime() {
		worker.calculateWeeklyPay(45);
		Assert.assertEquals("pay incorrect: " + worker.calculateWeeklyPay(45), 47500, worker.calculateWeeklyPay(45), 0.0);
	}
	
	@Test
	public void verify_cannot_negative_overtime() {
		worker.calculateWeeklyPay(35);
		Assert.assertEquals("pay incorrect: " + worker.calculateWeeklyPay(35), 35000, worker.calculateWeeklyPay(35), 0.0);
	}
}
