package com.techelevator;
	
	import org.junit.Before;
	import org.junit.Test;
	import org.junit.Assert;


public class SalaryWorkerTest {

		
		SalaryWorker worker;

		@Before
		public void setup() {
			worker = new SalaryWorker("John", "Doe", 52000);
		}
		
		@Test
		public void verify_first_name() {
			Assert.assertEquals("first name incorrect: " + worker.getFirstName(), "John", worker.getFirstName());
		}

		@Test
		public void verify_last_name() {
			Assert.assertEquals("last name incorrect: " + worker.getLastName(), "Doe", worker.getLastName());
		}
		
		@Test
		public void verify_annual_salary() {
			worker.getAnnualSalary();
			Assert.assertEquals("annual salary incorrect: " + worker.getAnnualSalary(), 52000, worker.getAnnualSalary(), 0.0);
		}
		
		@Test
		public void verify_pay() {
			worker.calculateWeeklyPay(5);
			Assert.assertEquals("pay incorrect: " + worker.calculateWeeklyPay(5), 1000, worker.calculateWeeklyPay(5), 0.0);
		}
			
}
