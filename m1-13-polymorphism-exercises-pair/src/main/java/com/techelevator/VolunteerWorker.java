package com.techelevator;

public class VolunteerWorker implements Worker {
	
	private String firstName;
	private String lastName;
	
	public VolunteerWorker(String firstName, String lastName) {
		this.lastName =lastName;
		this.firstName = firstName;
	}

	@Override
	public double calculateWeeklyPay(int hoursWorked) {
		double pay = hoursWorked * 0;
		return pay;
	}

	@Override
	public String getFirstName() {
		return this.firstName;
	}

	@Override
	public String getLastName() {
		return this.lastName;
	}

}
