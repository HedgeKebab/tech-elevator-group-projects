package com.techelevator;

public class SalaryWorker implements Worker{
	
	private double annualSalary;
	private String firstName;
	private String lastName;
	
	public SalaryWorker(String firstName, String lastName, double annualSalary) {
		this.firstName = firstName;
		this.annualSalary = annualSalary;
		this.lastName = lastName;
	}

	@Override
	public double calculateWeeklyPay(int hoursWorked) {
		double pay = this.annualSalary / 52;
		return pay;
	}

	@Override
	public String getFirstName() {
		return this.firstName;
	}

	@Override
	public String getLastName() {
		return this.lastName;
	}

	public double getAnnualSalary() {
		return this.annualSalary;
	}

}
