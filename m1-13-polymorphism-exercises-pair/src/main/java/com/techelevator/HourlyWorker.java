package com.techelevator;

public class HourlyWorker implements Worker{
	
	private double hourlyRate;
	private String firstName;
	private String lastName;
	
	public HourlyWorker(String firstName, String lastName, double hourlyRate) {
		this.hourlyRate = hourlyRate;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	@Override
	public double calculateWeeklyPay(int hoursWorked) {
		double pay = hourlyRate * hoursWorked;
		double overtime = hoursWorked - 40;
		if (overtime < 0) {
			return pay;
		}
		else {
			overtime *= 0.5;
			pay += hourlyRate * overtime ;
			return pay;
		}
	}

	@Override
	public String getFirstName() {
		return this.firstName;
	}

	@Override
	public String getLastName() {
		return this.lastName;
	}
	
	public double getHourlyRate() {
		return this.hourlyRate;
	}


}
