package com.techelevator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Program {

	public static void main(String[] args) {
		
		Worker[] workers = new Worker[] {new SalaryWorker("John", "Doe", 10000), new VolunteerWorker("Jane", "Doe"), new HourlyWorker("James", "Bond", 1000)};
		System.out.println("Employee         Hours Worked           Pay");
		System.out.println("------------------------------------------");
		for (Worker person : workers) {
			System.out.print(person.getLastName() + ", " + person.getFirstName()+ "  ");
			Random workedHours = new Random();
			int hours = workedHours.nextInt(80) + 0;
			System.out.print(hours + "  ");
			double newPay = person.calculateWeeklyPay(hours);
			System.out.println(newPay);
		}
	}

}
